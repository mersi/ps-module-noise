#include <TDirectoryFile.h>
#include <TFile.h>
#include <TList.h>
#include <TCanvas.h>
#include <TF1.h>
#include <string>
#include <vector>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <TMath.h>

TFile* _file0;
TObject* myObject;
TH1F* hsigma = nullptr;
TH1F* hmu = nullptr;
TCanvas* myCanvas;
std::vector<TH1F*> getCurves(int hybridId, std::string chipType, int chipId) {
  int iCurve;
  std::vector<TH1F*> result;
  std::string dirName = Form("Detector/Board_0/OpticalGroup_1/Hybrid_%d/%s_%d/Channel", hybridId, chipType.c_str(), chipId);
  TDirectoryFile* myDir = (TDirectoryFile*)_file0->Get(dirName.c_str());
  TList* myList = myDir->GetListOfKeys();
  for (iCurve = 0 ; iCurve < myList->GetSize(); ++iCurve) {
    TKey* myKey = (TKey*) myList->At(iCurve);
    myObject = myKey->ReadObj();
    if  (myObject && (std::string(myObject->ClassName()) == "TH1F")) {
      TH1F* myCurve = (TH1F*)myKey->ReadObj();
      result.push_back(myCurve);
    } else {
      std::cerr << "ERROR: I found a missing object, or non-TH1F" << std::endl;
    }
  }
  return result;
}

double noiseFunction(Double_t *x, Double_t *par) {
  double xx = x[0];
  double a = par[0];
  double sigma_a = par[1];
  double b = par[2];
  double sigma_b = par[3];
  double c = par[4];
  double sigma_c = par[5];
  double N = par[6];

  const double SQ2 = TMath::Sqrt(2);
  double xa = (xx-a)/SQ2/sigma_a;
  double xb = (xx-b)/SQ2/sigma_b;
  double xc = (xx-c)/SQ2/sigma_c;

  double term1 = 1 + TMath::Erf(xa);
  double term2 = 1 - TMath::Erf(xb);
  double term3 = 1 - TMath::Power(TMath::Erf(xc), 2);
    
  return 0.25 * term1 * term2 + N * term3;
}
void getAndFit(int hybridId, std::string chipType, int chipId, TH1D* myNoiseHist = nullptr) {
  auto myCurves = getCurves(hybridId, chipType, chipId);
  double min_range = 0;
  double max_range = 255;
  TF1 *myFunction = new TF1("myFunction", noiseFunction, min_range, max_range, 7);
  myFunction->SetParName(0, "a");
  myFunction->SetParName(1, "sigma_a");
  myFunction->SetParName(2, "b");
  myFunction->SetParName(3, "sigma_b");
  myFunction->SetParName(4, "c");
  myFunction->SetParName(5, "sigma_c");
  myFunction->SetParName(6, "N");
    
  // Set parameter limits for myFunction
  myFunction->SetParLimits(0, 0, 300);
  myFunction->SetParLimits(1, 0, 50);
  myFunction->SetParLimits(2, 0, 300);
  myFunction->SetParLimits(3, 0, 50);
  myFunction->SetParLimits(4, 0, 300);
  myFunction->SetParLimits(5, 0, 50);
  myFunction->SetParLimits(6, 0, 100);
    
  //int currentChannel = 0;
  int count=0,anti=0;
  for (auto aCurve : myCurves) {
    double firstX_aboveHalf = 0, lastX_belowHalf = 0, maxX = 0, firstX_at60PercentOfMax = 0;
    double maxContent = 0;
    
    // Derivatives
    double derivativeFirst = 0, derivativeLast = 0;

    bool foundFirst = false;
    bool foundLast=false;
    // Add the loop to set bin errors to 1 for bins with content > 1
    int Nbins = aCurve->GetNbinsX();
    for (int iBin = 1; iBin <= Nbins; ++iBin) {
      aCurve->SetBinError(iBin, 0.1);
    }
    for (int i = 1; i <= aCurve->GetNbinsX(); ++i) {  
      double binCenter = aCurve->GetBinCenter(i);
      double content = aCurve->GetBinContent(i);
      double prevContent = aCurve->GetBinContent(i-1);

      // Check if we've encountered the maximum value
      if (content > maxContent) {
	maxContent = content;
	maxX = binCenter;
      }

      // Find the first and last x where the curve crosses 0.5
      if (content > 0.5 && !foundFirst) {
	foundFirst = true;
	firstX_aboveHalf = binCenter;
	// Calculate the derivative at this point
	derivativeFirst = (content-prevContent) / (aCurve->GetBinWidth(i));
      }

      if (prevContent>0.5 && content < 0.5) {
	foundLast = true;
	lastX_belowHalf = aCurve->GetBinCenter(i-1);  // previous bin was the last above 0.5
	// Calculate the derivative at this point
	derivativeLast = (content-prevContent) / (aCurve->GetBinWidth(i));
      }
    }
    
    // Find the x value where the histogram reaches 0.6 times its maximum for x > x_max
    //for (int i = aCurve->FindBin(maxX); i <= aCurve->GetNbinsX(); ++i) {
    //if (aCurve->GetBinContent(i) > 0.5 * maxContent) {
    //firstX_at60PercentOfMax = aCurve->GetBinCenter(i);
    //break;
    //}
    //}
    
    double a = firstX_aboveHalf;
    double b = lastX_belowHalf;
    double c =  maxX ;
    double sigma_a =fabs(derivativeFirst);
    double sigma_b =fabs(derivativeLast) ;
    double sigma_c =fabs(derivativeFirst) ; //it should be around 0.140//firstX_at60PercentOfMax;
    double N = 40;
    // Set the parameters for the function
    myFunction->SetParameters(a, sigma_a, b, sigma_b, c, sigma_c, N); // Replace with actual values
    // Parameters to find during the scan
  
    // Printing the results
    /*std::cout << "First x value above 0.5: " << firstX_aboveHalf << std::endl;
      std::cout << "Derivative at this point: " << derivativeFirst << std::endl;
      std::cout << "Last x value below 0.5: " << lastX_belowHalf << std::endl;
      std::cout << "Derivative at this point: " << derivativeLast << std::endl;
      std::cout << "x value at maximum: " << maxX << std::endl;
      std::cout << "x value at 0.6 times maximum for x > x_max: " << firstX_at60PercentOfMax << std::endl;
    */   
             
    //  myFunction->SetParLimits(i, 0, 100);
    /*   TH1F* fittedHist = (TH1F*)aCurve->Clone();//Mr.mersi
	 fittedHist->SetName(Form("FittedHist_Channel%d", currentChannel));
	 TFile* outputRootFile = TFile::Open("histo.root", "RECREATE"); // Create or overwrite "histo.root" for writing
	 fittedHist->Write(); // Write the histogram to "histo.root"
	 outputRootFile->Close(); // Close "histo.root*/
            
    // std::cout << "Fitting channel " << currentChannel << "...\n";
    aCurve->Fit(myFunction, "Q");
    if(aCurve->Fit(myFunction, "Q") != 0){
      //  std::cout << "fit did not converge"<<endl;
      count++;
    }    
    else{
      //std::cout << "fit converged"<<endl;
      /*std::cout << "Parameters: " << "a=" << myFunction->GetParameter(0)
	<< " sigma_a=" << myFunction->GetParameter(1)
	<< " b=" << myFunction->GetParameter(2)
	<< " sigma_b=" << myFunction->GetParameter(3)
	<< " c=" << myFunction->GetParameter(4)
	<< " sigma_c=" << myFunction->GetParameter(5)
	<< " N=" << myFunction->GetParameter(6);
	std::cout << "Chisquare/NDF: " << myFunction->GetChisquare() << "/" << myFunction->GetNDF() << std::endl;*/
      anti++;
    }

    // Draw the fit on top of the histogram
    aCurve->Draw();
    myFunction->Draw("same");
    gStyle->SetOptFit(11111);
  } //main for loop 
  std::cout<<"non-convergent="<<count<<" "<<"convergent="<<anti<<endl;
  //currentChannel++;
} //main function

TFile* openFile(int injection) {
  return TFile::Open(Form("data/Hybrid_%d.root", injection));
}

void noise_chip() {
  _file0 = openFile(50);
  //getAndFit(2, "SSA", 0);
  getAndFit(2, "MPA", 8);
}

